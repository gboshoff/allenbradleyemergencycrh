﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibplctagWrapper;
using System.Threading;

namespace AllenBradley
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //  var tag = new Tag(txtIPAddress.Text,"1,0", CpuType.LGX, "PartLogTimeDint", DataType.Int32, 1);
            //    var tag = new Tag("192.168.0.252", CpuType.SLC, "PartLogTimeDint", DataType.Int32,1);

            Run();
        }

        public void Run()
        {

            const string PLC_IP = "192.168.0.252";
            const string PLC_PATH = "1,0";
            const CpuType PLC_TYPE = CpuType.LGX;

            var tag = new Tag(PLC_IP, PLC_PATH, PLC_TYPE, "PartLogTimeDint", DataType.Int32, 1);
            var tag1 = new Tag(PLC_IP, PLC_PATH, PLC_TYPE, "PartLogMarkerID", DataType.String, 1);
            var tag2 = new Tag(PLC_IP, PLC_PATH, PLC_TYPE, "PartLogCloseForce", DataType.Float32, 1);
            var tag3 = new Tag(PLC_IP, PLC_PATH, PLC_TYPE, "PartLogOpenForce", DataType.REAL, 1);
            var tag4 = new Tag(PLC_IP, PLC_PATH, PLC_TYPE, "PartLogDimension", DataType.REAL, 1);
            var tag5 = new Tag(PLC_IP, PLC_PATH, PLC_TYPE, "DataLoggingDone", DataType.INT, 1);
            var tag6 = new Tag(PLC_IP, PLC_PATH, PLC_TYPE, "PartLogDateTime", DataType.String, 1);
            var tag7 = new Tag(PLC_IP, PLC_PATH, PLC_TYPE, "DataLoggingLiveBit", DataType.INT, 1);

            //    PartLogMarkerDint(Dint) 10 digit code
            //PartLogCloseForce(Real / Float)
            //PartLogOpenForce(real)
            //PartLogDimension(real)
            var client = new Libplctag();
            client.AddTag(tag);
            client.AddTag(tag1);
            client.AddTag(tag2);
            client.AddTag(tag3);
            client.AddTag(tag4);
            client.AddTag(tag5);
            client.AddTag(tag6);
            client.AddTag(tag7);

            while (client.GetStatus(tag) == Libplctag.PLCTAG_STATUS_PENDING)
            {
                Thread.Sleep(100);
            }

            // if the status is not ok, we have to handle the error
            if (client.GetStatus(tag) != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "Error setting up tag internal state. Error " + client.DecodeError(client.GetStatus(tag)) + "";
                return;
            }
            if (client.GetStatus(tag1) != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "Error setting up tag internal state. Error " + client.DecodeError(client.GetStatus(tag1)) + "";
                return;
            }
            if (client.GetStatus(tag2) != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "Error setting up tag internal state. Error " + client.DecodeError(client.GetStatus(tag2)) + "";
                return;
            }
            if (client.GetStatus(tag3) != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "Error setting up tag internal state. Error " + client.DecodeError(client.GetStatus(tag3)) + "";
                return;
            }
            if (client.GetStatus(tag4) != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "Error setting up tag internal state. Error " + client.DecodeError(client.GetStatus(tag4)) + "";
                return;
            }

            if (client.GetStatus(tag5) != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "Error setting up tag internal state. Error " + client.DecodeError(client.GetStatus(tag5)) + "";
                return;
            }

            if (client.GetStatus(tag6) != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "Error setting up tag internal state. Error " + client.DecodeError(client.GetStatus(tag6)) + "";
                return;
            }

            if (client.GetStatus(tag7) != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "Error setting up tag internal state. Error " + client.DecodeError(client.GetStatus(tag7)) + "";
                return;
            }


            var result = client.ReadTag(tag, 15000);
            var result1 = client.ReadTag(tag1, 15000);
            var result2 = client.ReadTag(tag2, 15000);
            var result3 = client.ReadTag(tag3, 15000);
            var result4 = client.ReadTag(tag4, 15000);
            var result5 = client.ReadTag(tag5, 15000);
            var result6 = client.ReadTag(tag6, 15000);
            var result7 = client.ReadTag(tag7, 15000);


            // Check the read operation result
            if (result != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "ERROR: Unable to read the data! Got error code {" + result + "}: {" + client.DecodeError(result) + " ";
                return;
            }
            if (result1 != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "ERROR: Unable to read the data! Got error code {" + result1 + "}: {" + client.DecodeError(result1) + " ";
                return;
            }
            if (result2 != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "ERROR: Unable to read the data! Got error code {" + result2 + "}: {" + client.DecodeError(result2) + " ";
                return;
            }
            if (result3 != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "ERROR: Unable to read the data! Got error code {" + result3 + "}: {" + client.DecodeError(result3) + " ";
                return;
            }
            if (result4 != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "ERROR: Unable to read the data! Got error code {" + result4 + "}: {" + client.DecodeError(result4) + " ";
                return;
            }

            if (result5 != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "ERROR: Unable to read the data! Got error code {" + result5 + "}: {" + client.DecodeError(result5) + " ";
                return;
            }

            if (result6 != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "ERROR: Unable to read the data! Got error code {" + result6 + "}: {" + client.DecodeError(result6) + " ";
                return;
            }

            if (result7 != Libplctag.PLCTAG_STATUS_OK)
            {
                lblError.Text = "ERROR: Unable to read the data! Got error code {" + result7 + "}: {" + client.DecodeError(result6) + " ";
                return;
            }

            // Convert the data
            var TestDintArray = client.GetInt32Value(tag, 0); // multiply with tag.ElementSize to keep indexes consistant with the indexes on the plc

            int size = client.GetInt32Value(tag1, 0);
            int offset = DataType.Int32;
            string output = string.Empty;

            for (int i = 0; i < tag1.ElementCount; i++)
            {
                var sb = new StringBuilder();

                for (int j = 0; j < size; j++)
                {
                    sb.Append((char)client.GetUint8Value(tag1, (i * tag.ElementSize) + offset + j));
                }

                output = sb.ToString();
            }



            int size6 = client.GetInt32Value(tag6, 0);
            int offset6 = DataType.Int32;
            string output6 = string.Empty;

            for (int i = 0; i < tag6.ElementCount; i++)
            {
                var sb6 = new StringBuilder();

                for (int j = 0; j < size6; j++)
                {
                    sb6.Append((char)client.GetUint8Value(tag6, (i * tag.ElementSize) + offset6 + j));
                }

                output6 = sb6.ToString();
            }


            string  PartLogMarkerDint = output;
            var PartLogCloseForce = client.GetFloat32Value(tag2, 0);//, 2 * tag.ElementSize);
            var PartLogOpenForce = client.GetFloat32Value(tag3, 0);
            var PartLogDimension = client.GetFloat32Value(tag4, 0);
            var DataLoggingTrigger = client.GetBitValue(tag5, 0, 10000);
            var DataLoggingBit = client.GetBitValue(tag7, 0, 10000);

            // print to console
            richTextBox1.Text = richTextBox1.Text + "PartLogLive: " + TestDintArray + "\n";
            richTextBox1.Text = richTextBox1.Text + "PartLogMarker: " + PartLogMarkerDint + "\n";
            richTextBox1.Text = richTextBox1.Text + "PartLogCloseForce: " + PartLogCloseForce + "\n";
            richTextBox1.Text = richTextBox1.Text + "PartLogOpenForce: " + PartLogOpenForce + "\n";
            richTextBox1.Text = richTextBox1.Text + "PartLogDimension: " + PartLogDimension + "\n";
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.ScrollToCaret();

            if (DataLoggingBit== false)
            {
                client.SetBitValue(tag7, 0, true, 15000);

            }
            else
            {
                client.SetBitValue(tag7, 0, false, 15000);

            }

            if (!String.IsNullOrEmpty(PartLogMarkerDint))
            {
                DA da = new DA();
                string SQL = "Select TOP 1 PartLogMarker From PartInfo order by Id desc";
                DataSet ds = da.ExecuteQuery(SQL);
                string checkpart = "";
                if (ds.Tables[0].Rows.Count > 0)
                {
                    checkpart = ds.Tables[0].Rows[0]["PartLogMarker"].ToString();
                }
                if (string.IsNullOrEmpty(checkpart))
                {

                    SQL = "INSERT INTO PartInfo(PartLogLive,PartLogMarker,PartlogCloseForce,PartLogOpenForce,PartLogDimension,DateTimeStamp,PartLogDate)Values('" + TestDintArray + "','" + PartLogMarkerDint + "','" + PartLogCloseForce + "','" + PartLogOpenForce + "','" + PartLogDimension + "',GETDATE(),'" + output6 + "')";
                    da.ExecuteActionQuery(SQL);
                    client.SetBitValue(tag5, 0, true, 15000);

                }
                else
                {
                    if (Convert.ToInt32(checkpart) != Convert.ToInt32(PartLogMarkerDint))
                    {
                        SQL = "INSERT INTO PartInfo(PartLogLive,PartLogMarker,PartlogCloseForce,PartLogOpenForce,PartLogDimension,DateTimeStamp,PartLogDate)Values('" + TestDintArray + "','" + PartLogMarkerDint + "','" + PartLogCloseForce + "','" + PartLogOpenForce + "','" + PartLogDimension + "',GETDATE(),'" + output6 + "')";
                        da.ExecuteActionQuery(SQL);
                        client.SetBitValue(tag5, 0, true, 15000);


                    }

                }
            }
        }



        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            Run();
            timer1.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DA da = new DA();
            string sql = "SELECT * FROM PartInfo WHERE PartLogMarker = '" + txtSerial.Text + "'";
            DataSet ds = new DataSet();
            ds = da.ExecuteQuery(sql);

            if (ds.Tables[0].Rows.Count > 0)
            {
                dataGridView1.DataSource = ds.Tables[0].Copy();
                dataGridView1.Refresh();

            }
            else;
            {
                lblError.Text = "nothing found";  
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
