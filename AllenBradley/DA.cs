﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace AllenBradley
{
    class DA
    {
        public DataSet ExecuteQuery(string SQLStatement)
        {
            using (SqlConnection con = new SqlConnection())
            {

                con.ConnectionString = ConfigurationSettings.AppSettings["DataSource"] +
                                       ConfigurationSettings.AppSettings["Database"] +
                                       ConfigurationSettings.AppSettings["User"];
                con.Open();

                SqlDataAdapter sqlda = new SqlDataAdapter(SQLStatement, con);
                DataSet DS = new DataSet();
                sqlda.Fill(DS, "Data");
                con.Close();
                return DS;
            }
        }

        public void ExecuteActionQuery(string SQLStatement)
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationSettings.AppSettings["DataSource"] +
                                       ConfigurationSettings.AppSettings["Database"] +
                                       ConfigurationSettings.AppSettings["User"];

                SqlCommand myCommand = new SqlCommand(SQLStatement, con);
                myCommand.Connection.Open();
                myCommand.ExecuteNonQuery();
                con.Close();
            }
        }

    }
}
